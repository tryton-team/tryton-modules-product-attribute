tryton-modules-product-attribute (7.0.0-3) unstable; urgency=medium

  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:55:47 +0200

tryton-modules-product-attribute (7.0.0-2) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:23 +0200

tryton-modules-product-attribute (7.0.0-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Switch to pgpmode=none in the watch file.
  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083883).
  * Merging upstream version 7.0.0.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Oct 2024 14:23:28 +0200

tryton-modules-product-attribute (6.0.0-3) sid; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 13 Feb 2023 13:13:42 +0100

tryton-modules-product-attribute (6.0.0-2) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:53:02 +0100

tryton-modules-product-attribute (6.0.0-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.0.
  * Use same debhelper compat as for server and client.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:12:50 +0200

tryton-modules-product-attribute (5.0.3-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 11:33:15 +0200

tryton-modules-product-attribute (5.0.2-1) unstable; urgency=medium

  * Merging upstream version 5.0.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 10:17:48 +0200

tryton-modules-product-attribute (5.0.1-2) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:17:08 +0200

tryton-modules-product-attribute (5.0.1-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 12:41:52 +0100

tryton-modules-product-attribute (5.0.0-2) unstable; urgency=medium

  * Cleanup white space.
  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:35:17 +0100

tryton-modules-product-attribute (5.0.0-1) unstable; urgency=medium

  * Merging upstream version 5.0.0.
  * Updating copyright file.
  * Updating to Standards-Version: 4.2.1, no changes needed.
  * Update signing-key.asc with the minimized actual upstream maintainer
    key.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 20 Nov 2018 17:08:10 +0100

tryton-modules-product-attribute (4.6.0-2) unstable; urgency=medium

  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 29 Mar 2018 21:14:52 +0200

tryton-modules-product-attribute (4.6.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Merging upstream version 4.6.0.
  * Use https in the watch file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 07 Nov 2017 10:20:14 +0100

tryton-modules-product-attribute (4.4.0-4) unstable; urgency=medium

  * Switch to Python3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Aug 2017 12:35:28 +0200

tryton-modules-product-attribute (4.4.0-3) unstable; urgency=medium

  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 16:02:50 +0200

tryton-modules-product-attribute (4.4.0-2) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Jul 2017 12:36:12 +0200

tryton-modules-product-attribute (4.4.0-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 4.4.0.
  * Updating debian/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 10 Jun 2017 23:29:49 +0200

tryton-modules-product-attribute (4.2.0-1) unstable; urgency=medium

  * Updating to Standards-Version: 3.9.8, no changes needed.
  * Merging upstream version 4.2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 05 Dec 2016 15:31:23 +0100

tryton-modules-product-attribute (4.0.0-1) unstable; urgency=medium

  * Updating signing-key.asc with the actual upstream maintainer keys.
  * Merging upstream version 4.0.0.
  * Updating the copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 30 May 2016 19:29:38 +0200

tryton-modules-product-attribute (3.8.0-2) unstable; urgency=medium

  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.
  * Disable tests globally for all Python versions.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 13:09:00 +0100

tryton-modules-product-attribute (3.8.0-1) unstable; urgency=medium

  * Updating year of debian copyright.
  * Adapting section naming in gbp.conf to current git-buildpackage.
  * Improving description why we can not run the module test suites.
  * Merging upstream version 3.8.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 12 Nov 2015 19:13:34 +0100

tryton-modules-product-attribute (3.6.0-1) unstable; urgency=medium

  * Wrapping and sorting control files (wrap-and-sort -bts).
  * Merging upstream version 3.6.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 26 Apr 2015 23:49:23 +0200

tryton-modules-product-attribute (3.4.1-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Improving boilerplate in d/control (Closes: #771722).
  * Merging upstream version 3.4.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 24 Feb 2015 19:11:47 +0100

tryton-modules-product-attribute (3.4.0-1) unstable; urgency=medium

  * Updating signing key while using now plain .asc files instead of .pgp
    binaries.
  * Adding actual upstream signing key.
  * Updating to Standards-Version: 3.9.6, no changes needed.
  * Merging upstream version 3.4.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 21 Oct 2014 20:24:12 +0200

tryton-modules-product-attribute (3.2.0-1) unstable; urgency=medium

  * Removing  LC_ALL=C.UTF-8 as build environment.
  * Merging upstream version 3.2.0.
  * Updating copyright.
  * Bumping minimal required Python version to 2.7.
  * Updating gbp.conf for usage of upstream tarball compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 24 Apr 2014 15:27:59 +0200

tryton-modules-product-attribute (3.0.0-3) unstable; urgency=medium

  * Updating year in debian copyright.
  * Removing debian/source/options, we are building with dpkg defaults.
  * Removing PYBUILD_DESTDIR_python2 from rules, it is no more needed.
  * Adding pgp verification for uscan.
  * Adding gbp.conf for usage with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Mar 2014 17:15:41 +0100

tryton-modules-product-attribute (3.0.0-2) unstable; urgency=low

  * Pointing VCS fields to new location on alioth.debian.org.
  * Using dpkg defaults for xz compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 02 Dec 2013 21:16:37 +0100

tryton-modules-product-attribute (3.0.0-1) unstable; urgency=low

  * Merging upstream version 3.0.0.
  * Updating to standards version 3.9.5, no changes needed.
  * Changing to buildsystem pybuild.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 25 Nov 2013 17:54:29 +0100

tryton-modules-product-attribute (2.8.0-2) unstable; urgency=low

  * Moving doc/index.rst to appropriate subdirectory doc.
  * Simplifying package layout by renaming <pkg_name>.docs to docs.
  * Removing needless empty line in rules.
  * Adapting the rules file to work also with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 06 Aug 2013 13:33:07 +0200

tryton-modules-product-attribute (2.8.0-1) experimental; urgency=low

  * Initial packaging (Closes: #706835).

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 02 May 2013 13:30:25 +0200
